'use strict';

jest.dontMock('../src/utils/validator');

describe('ValidatorUtils', function() {
    it('Validates string field if is string', function() {
        var ValidatorUtils = require('../src/utils/validator');

        var result = ValidatorUtils.validateStringField('mock', null);

        // Validator must return error when field is not a string
        expect(typeof result).toBe('object');
        expect(result.field).toBe('mock');
        expect(result.message).toBe('Mock must be of type `string`.');
    });

    it('Validates string field min-length', function() {
        var ValidatorUtils = require('../src/utils/validator');

        var result = ValidatorUtils.validateStringField('mock', '', {minLength: 1});
        var result1 = ValidatorUtils.validateStringField('mock', 'm', {minLength: 2});

        // Validator must return specific error when field is smaller than the min-length
        expect(typeof result).toBe('object');
        expect(result.field).toBe('mock');
        expect(result.message).toBe('Mock cannot be empty.');

        expect(typeof result1).toBe('object');
        expect(result1.field).toBe('mock');
        expect(result1.message).toBe('Mock must be larger than 2 characters.');
    });

    it('Validates string field max-length', function() {
        var ValidatorUtils = require('../src/utils/validator');

        var result = ValidatorUtils.validateStringField('mock', 'aa', {maxLength: 1});

        // Validator must return specific error when field is larger than the max-length
        expect(typeof result).toBe('object');
        expect(result.field).toBe('mock');
        expect(result.message).toBe('Mock cannot be larger than 1 characters.');
    });
});
