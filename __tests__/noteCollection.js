'use strict';

jest.dontMock('../src/client/js/collections/NoteCollection');
jest.dontMock('../src/utils/config');

describe('NoteCollection', function() {
    it('Synchronizes when initialized', function() {
        var request = require('superagent');
        var NoteCollection = require('../src/client/js/collections/NoteCollection');

        // Method GET must be called with correct url
        expect(request.get).toBeCalledWith(NoteCollection.url);

        // Sync must return two notes
        // @see __mocks__/superagent.js
        expect(NoteCollection.toJSON().length).toBe(2);
    });

    it('Creates and saves a note', function() {
        var request = require('superagent');

        var NoteModel = require('../src/client/js/models/NoteModel');
        var NoteCollection = require('../src/client/js/collections/NoteCollection');

        var mockNote = {
            title: 'mock',
            body: 'mock',
            tags: undefined
        };

        // Reset the collection so we have only one note
        NoteCollection.reset();
        NoteCollection.createNote(mockNote.title, mockNote.body);

        // Validation must be called
        expect(NoteModel.prototype.validate).toBeCalled();

        // Method POST must be called with correct url
        expect(request.post).toBeCalledWith(NoteCollection.url);

        // Method SEND must include the note
        expect(request.send).toBeCalledWith(mockNote);

        // Note must be created and pushed to the collection
        expect(NoteCollection.toJSON().length).toBe(1);
    });

    it('Updates a specific note', function() {
        var request = require('superagent');

        var NoteModel = require('../src/client/js/models/NoteModel');
        var NoteCollection = require('../src/client/js/collections/NoteCollection');

        var mockNote = {
            id: 'mock',
            title: 'mock',
            body: 'mock',
            tags: undefined
        };

        // Reset the collection so we have only one note
        NoteCollection.reset();
        NoteCollection.set(new NoteModel(mockNote));

        // Updated values
        mockNote.title = 'mock1';
        mockNote.body = 'mock1';

        NoteCollection.updateNote(mockNote.id, mockNote.title, mockNote.body);

        // Validation must be called
        expect(NoteModel.prototype.validate).toBeCalled();

        // Method PUT must be called with correct url
        expect(request.put).toBeCalledWith(NoteCollection.url + '/' + mockNote.id);

        // Method SEND must include the updated note
        expect(request.send).toBeCalledWith(mockNote);

        // Note must be updated and the collection must only have one note
        expect(NoteCollection.toJSON().length).toBe(1);

        // Note fields must be updated
        expect(NoteCollection.get(mockNote.id).toJSON().title).toBe('mock1');
        expect(NoteCollection.get(mockNote.id).toJSON().body).toBe('mock1');
    });

    it('Deletes a specific note', function() {
        var request = require('superagent');

        var NoteModel = require('../src/client/js/models/NoteModel');
        var NoteCollection = require('../src/client/js/collections/NoteCollection');

        var mockNote = {
            id: 'mock',
            title: 'mock',
            body: 'mock',
            tags: undefined
        };

        // Reset the collection so we have only one note
        NoteCollection.reset();
        NoteCollection.set(new NoteModel(mockNote));

        NoteCollection.deleteNote(mockNote.id);

        // Method DELETE must be called with correct url
        expect(request.del).toBeCalledWith(NoteCollection.url + '/' + mockNote.id);

        // Note must be deleted from the collection
        expect(NoteCollection.toJSON().length).toBe(0);
    });
});
