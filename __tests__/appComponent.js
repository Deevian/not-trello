'use strict';

jest.dontMock('../src/client/js/models/globals/SearchModel');

jest.dontMock('../src/client/js/components/App');
jest.dontMock('../src/client/js/components/Header');
jest.dontMock('../src/client/js/components/NoteList');

jest.dontMock('../src/utils/tag');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');

var NoteCollection = require('../src/client/js/collections/NoteCollection');

var AppComponent = require('../src/client/js/components/App');
var HeaderComponent = require('../src/client/js/components/Header');
var NoteListComponent = require('../src/client/js/components/NoteList');

describe('AppComponent', function() {
    it('Renders a Header and a NoteList', function() {
        var app = TestUtils.renderIntoDocument(<AppComponent />);

        // App must render a Header component
        expect(TestUtils.scryRenderedComponentsWithType(app, HeaderComponent).length).toBe(1);

        // App must render a NoteList component
        expect(TestUtils.scryRenderedComponentsWithType(app, NoteListComponent).length).toBe(1);
    });

    it('Filter notes by a searchFilter', function() {
        var app = TestUtils.renderIntoDocument(<AppComponent />);
        var noteList = TestUtils.findRenderedComponentWithType(app, NoteListComponent);

        var mockSearchFilter = 'mock1,mock3';

        // Without filter, it must render everything
        expect(noteList.props.notes.length).toBe(NoteCollection.length);

        // With filter, it must render only render notes that have the same tags
        // @see src/client/js/collections/__mocks__/NoteCollection.js
        app.setState({search: {searchFilter: mockSearchFilter}});
        expect(noteList.props.notes.length).toBe(1);
    });
});
