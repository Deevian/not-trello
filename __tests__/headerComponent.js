'use strict';

jest.dontMock('../src/client/js/models/globals/SearchModel');
jest.dontMock('../src/client/js/components/Header');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');

var HeaderComponent = require('../src/client/js/components/Header');
var SearchModel = require('../src/client/js/models/globals/SearchModel');

describe('HeaderComponent', function() {
    it('Set search on click', function() {
        var header = TestUtils.renderIntoDocument(<HeaderComponent />);
        var searchInput = TestUtils.findRenderedDOMComponentWithTag(header, 'input');

        // Search filter must be undefined by default
        var searchFilter = SearchModel.get('searchFilter');
        expect(searchFilter).toBe(null);

        searchInput.value = 'mock';
        TestUtils.Simulate.click(TestUtils.findRenderedDOMComponentWithClass(header, 'btn'));

        // Search filter must have changed
        expect(SearchModel.get('searchFilter')).toBe(searchInput.value);

        // Reset model
        SearchModel.set({searchFilter: null});
    });

    it('Set search on enter press', function() {
        var header = TestUtils.renderIntoDocument(<HeaderComponent />);
        var searchInput = TestUtils.findRenderedDOMComponentWithTag(header, 'input');

        // Search filter must be undefined by default
        var searchFilter = SearchModel.get('searchFilter');
        expect(searchFilter).toBe(null);

        searchInput.value = 'mock';
        TestUtils.Simulate.keyPress(searchInput, {key: 'Enter', keyCode: 13, which: 13});

        // Search filter must have changed
        expect(SearchModel.get('searchFilter')).toBe(searchInput.value);

        // Reset model
        SearchModel.set({searchFilter: null});
    });
});
