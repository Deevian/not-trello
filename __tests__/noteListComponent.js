'use strict';

jest.dontMock('react-notification-system');

jest.dontMock('../src/client/js/components/NoteList');
jest.dontMock('../src/client/js/components/Note');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');

var NotificationSystem = require('react-notification-system');

var NoteCollection = require('../src/client/js/collections/NoteCollection');
var NoteListComponent = require('../src/client/js/components/NoteList');
var NoteComponent = require('../src/client/js/components/Note');

describe('NoteListComponent', function() {
    it('Renders NotificationSystem', function() {
        var noteList = TestUtils.renderIntoDocument(<NoteListComponent notes={NoteCollection.toJSON()} />);

        // NoteList must render a NotificationSystem
        expect(TestUtils.scryRenderedComponentsWithType(noteList, NotificationSystem).length).toBe(1);
    });

    it('Renders notes', function() {
        var noteList = TestUtils.renderIntoDocument(<NoteListComponent notes={NoteCollection.toJSON()} />);

        var result = TestUtils.scryRenderedComponentsWithType(noteList, NoteComponent);

        // NoteList must render at least one note
        expect(result.length).toBeGreaterThan(1);

        // The first note to be rendered must have a formOnly prop
        expect(result[0].props.formOnly).toBeTruthy();
    });
});
