'use strict';

jest.dontMock('superagent');

jest.dontMock('../src/utils/response');
jest.dontMock('../src/utils/config');

/**
 * Not really sure what's the deal with Jest and him/her ignoring some dontMock calls
 * But this fixes it.
 */
jest.autoMockOff();
var request = require.requireActual('superagent');
jest.autoMockOn();

describe('API', function() {
    it('It serves all the notes', function() {
        var ConfigUtils = require('../src/utils/config');

        request.get(ConfigUtils.api.url + '/notes')
            .end(function(err, results) {
                // There must be no connection errors
                expect(err).toBeFalsy();

                // API must return a successful response
                expect(results.body.success).toBeTruthy();
                expect(results.body.code).toBe(200);

                // API must return an array as the body
                expect(Array.isArray(results.body.data)).toBeTruthy();
            });
    });

    it('It creates a note', function() {
        var ConfigUtils = require('../src/utils/config');

        var mockTitle = 'mock';
        var mockBody = 'mock';

        request.post(ConfigUtils.api.url + '/notes')
            .send({title: mockTitle, body: mockBody, mock: true})
            .end(function(err, results) {
                // There must be no connection errors
                expect(err).toBeFalsy();

                // API must return a successful response
                expect(results.body.success).toBeTruthy();
                expect(results.body.code).toBe(201);

                // API must return the created note as body
                expect(results.body.data.id).toBeTruthy();
                expect(results.body.data.title).toBe(mockTitle);
                expect(results.body.data.title).toBe(mockBody);
            });
    });

    it('It updates a note', function() {
        var ConfigUtils = require('../src/utils/config');

        var mockId = 'mock';
        var mockTitle = 'mock1';
        var mockBody = 'mock1';

        request.put(ConfigUtils.api.url + '/notes/' + mockId)
            .send({id: mockId, title: mockTitle, body: mockBody, mock: true})
            .end(function(err, results) {
                // There must be no connection errors
                expect(err).toBeFalsy();

                // API must return a successful response
                expect(results.body.success).toBeTruthy();
                expect(results.body.code).toBe(200);

                // API must return the updated note as body
                expect(results.body.data.id).toBeTruthy();
                expect(results.body.data.title).toBe(mockTitle);
                expect(results.body.data.title).toBe(mockBody);
            });
    });

    it('It deletes a note', function() {
        var ConfigUtils = require('../src/utils/config');

        var mockId = 'mock';

        request.del(ConfigUtils.api.url + '/notes/' + mockId)
            .send({mock: true})
            .end(function(err, results) {
                // There must be no connection errors
                expect(err).toBeFalsy();

                // API must return a successful response
                expect(results.body.success).toBeTruthy();
                expect(results.body.code).toBe(200);
            });
    });
});
