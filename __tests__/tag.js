'use strict';

jest.dontMock('../src/utils/tag');

describe('TagUtils', function() {
    it('Expands a comma-separated tag string', function() {
        var TagUtils = require('../src/utils/tag');

        var tagString = ',first,second';

        // It must split by comma and include the empty tag
        expect(TagUtils.expand(tagString).length).toBe(3);

        // If empty string is passed, it must return an empty array
        expect(Array.isArray(TagUtils.expand(null))).toBeTruthy();
        expect(TagUtils.expand(null).length).toBe(0);
    });

    it('Normalizes a tag array', function() {
        var TagUtils = require('../src/utils/tag');

        var tagArray = ['  one', '  one', 'TWO'];
        var result = TagUtils.normalizeExpanded(tagArray);

        // Normalization must result in an array with two keys
        expect(result.length).toBe(2);

        // First tag empty space must be removed
        expect(result[0]).toBe('one');

        // Second tag must be lowercase
        expect(result[1]).toBe('two');
    });
});
