'use strict';

jest.dontMock('../src/client/js/components/Note');

var React = require('react');
var ReactDOM = require('react-dom');
var TestUtils = require('react-addons-test-utils');

var NoteCollection = require('../src/client/js/collections/NoteCollection');
var NoteComponent = require('../src/client/js/components/Note');

describe('NoteComponent', function() {
    it('Renders Note normally', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent note={NoteCollection.get('mock').toJSON()} />);

        // Note must render an article with the class normalNote
        expect(TestUtils.scryRenderedDOMComponentsWithClass(note, 'normalNote').length).toBe(1);
    });

    it('Renders form when it has prop formOnly', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent formOnly note={NoteCollection.get('mock').toJSON()} />);

        // Note must render an article with the class formNote
        expect(TestUtils.scryRenderedDOMComponentsWithClass(note, 'formNote').length).toBe(1);
    });

    it('Renders form when it has state editMode', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent note={NoteCollection.get('mock').toJSON()} />);

        // Note must render an article with the class formNote
        note.setState({editMode: true});
        expect(TestUtils.scryRenderedDOMComponentsWithClass(note, 'formNote').length).toBe(1);
    });

    it('Deletes note when delete button is clicked', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent note={NoteCollection.get('mock').toJSON()} />);

        // Delete method in NoteCollection must be called
        TestUtils.Simulate.click(TestUtils.findRenderedDOMComponentWithClass(note, 'deleteBtn'));
        expect(NoteCollection.deleteNote).toBeCalledWith(note.props.note.id);
    });

    it('Creates note when create button is clicked', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent formOnly note={NoteCollection.get('mock').toJSON()} />);

        var title = note.refs.titleInput.value;
        var body = note.refs.bodyInput.value;
        var tags = note.refs.tagsInput.value;

        // Create method in NoteCollection must be called
        TestUtils.Simulate.click(TestUtils.findRenderedDOMComponentWithClass(note, 'createBtn'));
        expect(NoteCollection.createNote).toBeCalledWith(title, body, tags);
    });

    it('Creates note when enterKey is pressed in title input', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent formOnly note={NoteCollection.get('mock').toJSON()} />);

        var title = note.refs.titleInput.value;
        var body = note.refs.bodyInput.value;
        var tags = note.refs.tagsInput.value;

        // Create method in NoteCollection must be called
        TestUtils.Simulate.keyPress(note.refs.titleInput, {key: 'Enter', keyCode: 13, which: 13});
        expect(NoteCollection.createNote).toBeCalledWith(title, body, tags);
    });

    it('Creates note when enterKey is pressed in tags input', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent formOnly note={NoteCollection.get('mock').toJSON()} />);

        var title = note.refs.titleInput.value;
        var body = note.refs.bodyInput.value;
        var tags = note.refs.tagsInput.value;

        // Create method in NoteCollection must be called
        TestUtils.Simulate.keyPress(note.refs.titleInput, {key: 'Enter', keyCode: 13, which: 13});
        expect(NoteCollection.createNote).toBeCalledWith(title, body, tags);
    });

    it('Changes to editMode when edit button is clicked', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent note={NoteCollection.get('mock').toJSON()} />);

        // Note must render an article with the class formNote
        TestUtils.Simulate.click(TestUtils.findRenderedDOMComponentWithClass(note, 'editBtn'));
        expect(TestUtils.scryRenderedDOMComponentsWithClass(note, 'formNote').length).toBe(1);
    });

    it('Confirms changes when confirmEdit button is clicked', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent note={NoteCollection.get('mock').toJSON()} />);

        var title;
        var body;
        var tags;

        note.setState({editMode: true});

        title = note.refs.titleInput.value;
        body = note.refs.bodyInput.value;
        tags = note.refs.tagsInput.value;

        // Update method in NoteCollection must be called
        TestUtils.Simulate.click(TestUtils.findRenderedDOMComponentWithClass(note, 'confirmEditBtn'));
        expect(NoteCollection.updateNote).toBeCalledWith(note.props.note.id, title, body, tags);
    });

    it('Confirms changes when enterKey is pressed in title input', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent note={NoteCollection.get('mock').toJSON()} />);

        var title;
        var body;
        var tags;

        note.setState({editMode: true});

        title = note.refs.titleInput.value;
        body = note.refs.bodyInput.value;
        tags = note.refs.tagsInput.value;

        // Update method in NoteCollection must be called
        TestUtils.Simulate.keyPress(note.refs.titleInput, {key: 'Enter', keyCode: 13, which: 13});
        expect(NoteCollection.updateNote).toBeCalledWith(note.props.note.id, title, body, tags);
    });

    it('Confirms changes when enterKey is pressed in tags input', function() {
        var note = TestUtils.renderIntoDocument(<NoteComponent note={NoteCollection.get('mock').toJSON()} />);

        var title;
        var body;
        var tags;

        note.setState({editMode: true});

        title = note.refs.titleInput.value;
        body = note.refs.bodyInput.value;
        tags = note.refs.tagsInput.value;

        // Update method in NoteCollection must be called
        TestUtils.Simulate.keyPress(note.refs.tagsInput, {key: 'Enter', keyCode: 13, which: 13});
        expect(NoteCollection.updateNote).toBeCalledWith(note.props.note.id, title, body, tags);
    });
});
