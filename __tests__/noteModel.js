'use strict';

jest.dontMock('../src/utils/validator');
jest.dontMock('../src/client/js/models/NoteModel');

describe('NoteModel', function() {
    it('Validates fields min-length', function() {
        var NoteModel = require('../src/client/js/models/NoteModel');

        var mockNote = {
            title: '',
            body: '',
            tags: ['']
        };

        var note = new NoteModel(mockNote);
        var result = note.validate();

        // Validation must result in two errors
        expect(result.length).toBe(2);

        // Errors should be in the body and in the first tag
        expect(result[0].field).toBe('body');
        expect(result[1].field).toBe('tag');
    });

    it('Validates fields max-length', function() {
        var NoteModel = require('../src/client/js/models/NoteModel');

        var mockNote = {
            title: 'mockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockm',
            body: 'mockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmocmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmockmo',
            tags: ['mockmockmockmockmockmockmockmockmockmockm']
        };

        var note = new NoteModel(mockNote);
        var result = note.validate();

        // Validation must return tree errors
        expect(result.length).toBe(3);

        // Errors must include every field
        expect(result[0].field).toBe('title');
        expect(result[1].field).toBe('body');
        expect(result[2].field).toBe('tag');
    });
});
