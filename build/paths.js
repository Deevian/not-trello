'use strict';

var path = require('path');

var build = path.resolve(__dirname);
var base = path.resolve(build, '..');

var src = path.join(base, 'src');
var dist = path.join(base, 'dist');

module.exports = {
    base: base,
    build: build,
    src: src,
    dist: dist,
    src_client_js: path.join(src, 'client', 'js'),
    src_client_scss: path.join(src, 'client', 'scss'),
    src_server: path.join(src, 'server'),
    build_webpack: path.join(build, 'webpack'),
    build_tasks: path.join(build, 'tasks')
};
