'use strict';

var webpack = require('webpack');
var _ = require('lodash');

var paths = require('../paths');

var config = {
    debug: false,
    devtool: 'hidden-source-map',
    entry: [
        paths.src_client_js + '/main'
    ],
    output: {
        path: paths.dist,
        publicPath: '/dist/',
        filename: 'bundle.js'
    },
    resolve: {
        root: paths.base,
        extensions: ['', '.js', '.jsx', '.json'],
        alias: {
            app: paths.src_client_js,
            scss: paths.src_client_scss
        }
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('common.js'),

        new webpack.optimize.UglifyJsPlugin({minimize: true}),
        new webpack.optimize.OccurenceOrderPlugin(),

        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.MinChunkSizePlugin({minChunkSize: 10000}),
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 15})
    ],
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loaders: ["style", "css", "sass"]
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['react']
                }
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            }
        ]
    },
    sassLoader: {
        includePaths: [paths.src_css],
        outputStyle: 'compressed'
    }
};

if (process.env.NODE_ENV === process.env.ENV_DEV) {
    config = _.merge(config, require('./dev.config.js'));
}

module.exports = config;
