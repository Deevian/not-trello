'use strict';

var webpack = require('webpack');

var paths = require('../paths');

var config = {
    debug: true,
    devtool: 'source-map',
    plugins: [
        new webpack.optimize.CommonsChunkPlugin('common.js'),

        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.NoErrorsPlugin(),

        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.MinChunkSizePlugin({minChunkSize: 10000}),
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 15})

    ],
    sassLoader: {
        includePaths: [paths.src_css],
        outputStyle: 'nested',
        sourceComments: true
    }
};

module.exports = config;
