'use strict';

var fs = require('fs');
var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');

var paths = require('../paths');

gulp.task('build', function(callback) {
    var webpackConfig;

    webpackConfig = require(paths.build_webpack + '/config.js');
    webpack(webpackConfig, function(err, stats) {
        if (err) {
            throw new gutil.PluginError('webpack', err);
        }

        gutil.log('[webpack]', stats.toString({colors: true}));
        callback();
    });
});
