'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var webpack = require('webpack');
var webpackDevMiddleware = require('webpack-dev-middleware');
var historyApiFallback = require('connect-history-api-fallback');
var compress = require('compression');

var paths = require('../paths');

gulp.task('serve', ['clean'], function(done) {
    var webpackConfig;
    var bundler;

    webpackConfig = require(paths.build_webpack + '/config.js');
    bundler = webpack(webpackConfig);

    browserSync({
        open: false,
        port: 8080,
        server: {
            baseDir: ['.'],
            index: 'index.html',
            middleware: [
                function(req, res, next) {
                    res.setHeader('Access-Control-Allow-Origin', '*');
                    next();
                },

                historyApiFallback(),
                compress(),

                webpackDevMiddleware(bundler, {
                    publicPath: '/dist/',
                    stats: {
                        colors: true
                    }
                })
            ]
        }
    }, done);
});

