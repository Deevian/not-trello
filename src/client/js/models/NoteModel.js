'use strict';

var Backbone = require('backbone');
var _ = require('lodash');

/**
 * Relative path for usage in tests and server
 * @TODO Package the utils
 */
var ValidatorUtils = require('../../../utils/validator');

var NoteModel = Backbone.Model.extend({
    defaults: {
        id: '',
        title: '',
        body: '',
        tags: []
    },

    validations: {
        title: {
            minLength: 0,
            maxLength: 120
        },
        body: {
            minLength: 1,
            maxLength: 1000
        },
        tag: {
            minLength: 1,
            maxLength: 40
        }
    },

    /**
     * @returns {boolean|{errors:array}}
     */
    validate: function() {
        var titleValidation;
        var bodyValidation;
        var tagValidation;
        var errors;

        errors = [];

        titleValidation = this.validateTitle();
        if (titleValidation !== true) {
            errors.push(titleValidation);
        }

        bodyValidation = this.validateBody();
        if (bodyValidation !== true) {
            errors.push(bodyValidation);
        }

        tagValidation = this.validateTags();
        if (tagValidation !== true) {
            errors = errors.concat(tagValidation);
        }

        if (errors.length !== 0) {
            return errors;
        }

        return true;
    },

    /**
     * @returns {boolean|{field: string, message: string}}
     */
    validateTitle: function() {
        return ValidatorUtils.validateStringField('title', this.get('title'), this.validations.title);
    },

    /**
     * @returns {boolean|{field: string, message: string}}
     */
    validateBody: function() {
        return ValidatorUtils.validateStringField('body', this.get('body'), this.validations.body);
    },

    /**
     * @returns {boolean|{field: string, message: string}}
     */
    validateTags: function() {
        var tagValidation;

        tagValidation = true;
        _.forEach(this.get('tags'), function(tag) {
            tagValidation = this._validateTag(tag);
            if (tagValidation !== true) {
                return false;
            }

            return true;
        }.bind(this));

        return tagValidation;
    },

    /**
     * Individual tag validation
     *
     * @param tagName
     * @returns {boolean|{field: string, message: string}}
     * @private
     */
    _validateTag: function(tagName) {
        return ValidatorUtils.validateStringField('tag', tagName, this.validations.tag);
    }
});

module.exports = NoteModel;
