'use strict';

var Backbone = require('backbone');

var NoteModel = Backbone.Model.extend({
    validate: jest.genMockFunction().mockImplementation(function() {
        return true;
    }),

    validateTitle: jest.genMockFunction().mockImplementation(function() {
        return true;
    }),

    validateBody: jest.genMockFunction().mockImplementation(function() {
        return true;
    }),

    validateTags: jest.genMockFunction().mockImplementation(function() {
        return true;
    })
});

module.exports = NoteModel;
