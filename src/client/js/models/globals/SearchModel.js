'use strict';

var Backbone = require('backbone');

var SearchModel = Backbone.Model.extend({
    defaults: {
        searchFilter: null
    }
});

module.exports = new SearchModel();
