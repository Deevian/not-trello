'use strict';

var React = require('react');

var SearchModel = require('app/models/globals/SearchModel');

var Header = React.createClass({
    styles: {
        header: {
            padding: '5rem 0 1rem 0'
        },
        title: {
            paddingLeft: 0
        },
        search: {
            marginTop: '0.45rem',
            padding: 0
        }
    },

    formPlaceholders: {
        tagSearch: 'search by tags (comma-separated)'
    },

    /**
     * @returns {XML}
     */
    render: function() {
        return (
            <header style={this.styles.header} className="row">
                <h1 className="col-xs-6" style={this.styles.title}>Note Board</h1>

                <div className="col-md-5 col-xs-6 pull-right input-group" style={this.styles.search}>
                    <input
                        ref="searchInput"
                        type="text"
                        className="form-control"
                        placeholder={this.formPlaceholders.tagSearch}

                        onKeyPress={this._onKeyPress}
                    />
                    <div className="btn btn-secondary  input-group-addon" onClick={this._onSearchClick}>
                        <i className="fa fa-search" />
                    </div>
                </div>
            </header>
        );
    },

    /**
     * @returns {boolean}
     * @private
     */
    _onSearchClick: function() {
        var search;

        // Set the search in the model so that the App will pick up the change
        search = this.refs.searchInput.value;
        SearchModel.set({searchFilter: search});

        return true;
    },

    /**
     * @param {event} evt
     * @returns {boolean}
     * @private
     */
    _onKeyPress: function(evt) {
        if (evt.key === 'Enter') {
            return this._onSearchClick(evt);
        }

        return false;
    },
});

module.exports = Header;
