'use strict';

var React = require('react');
var marked = require('marked');

var NoteCollection = require('app/collections/NoteCollection');

var Note = React.createClass({
    propTypes: {
        note: React.PropTypes.object,
        notificationSystem: React.PropTypes.object,
        formOnly: React.PropTypes.bool
    },

    styles: {
        article: {
            padding: 0
        },
        articleBody: {
            margin: '0.1rem',
            border: '2px solid #DDDDDD',
            backgroundColor: '#EEEEEE',
            minHeight: '9em'
        },
        textField: {
            padding: '0.5rem 0.4rem 0.4rem 0.8rem'
        },
        btnGroupWrapper: {
            padding: '0.5rem 0.5rem 0.45rem'
        },
        button: {
            fontSize: '0.75rem',
            padding: '0.2rem 0.5rem'
        },
        tag: {
            marginRight: '0.2rem',
            marginBottom: '0.2rem'
        },
        form: {
            title: {
                padding: '0 0.5rem',
                marginTop: '0.5rem'
            },
            body: {
                minHeight: '3.5rem',
                height: '3.5rem',
                padding: '0 0.5rem',
                marginBottom: '0.45rem'
            },
            tags: {
                padding: '0 0.5rem',
                marginBottom: '0.4rem'
            }
        }
    },

    formPlaceholders: {
        title: 'What\'s it\'s name?',
        body: 'What\'s it about?',
        tags: 'fun, work... (comma-separated)'
    },

    /**
     * @returns {{editMode: boolean, isEditing: boolean, isCreating: boolean, isDeleting: boolean}}
     */
    getInitialState: function() {
        return {
            editMode: false,
            isEditing: false,
            isCreating: false,
            isDeleting: false
        };
    },

    /**
     * @returns {boolean}
     */
    componentDidMount: function() {
        if (this.props.note) {
            this.setState(this.props.note);
        }

        return true;
    },

    /**
     * @returns {XML}
     */
    render: function() {
        if (this.state.editMode || this.props.formOnly) {
            return this._renderForm();
        }

        return this._renderNote();
    },

    /**
     * @returns {XML}
     * @private
     */
    _renderNote: function() {
        return (
            <article className="normalNote col-lg-3 col-md-4" style={this.styles.article}>
                <div className="container" style={this.styles.articleBody}>
                    <header className="row">
                        <h5 className="col-lg-8 col-md-8 col-sm-10 col-xs-9" style={this.styles.textField}>
                            {this.props.note.title}
                        </h5>

                        <div className="col-lg-4 col-md-4 col-sm-2 col-xs-3" style={this.styles.btnGroupWrapper}>
                            <span className="btn-group pull-right">
                                 <button
                                     type="button"
                                     className="editBtn btn btn-primary"
                                     style={this.styles.button}
                                     onClick={this._onEditClick}
                                 >
                                     <i className="fa fa-edit" />
                                 </button>

                                <button
                                    type="button"
                                    className="deleteBtn btn btn-danger"
                                    style={this.styles.button}
                                    onClick={this._onDeleteClick}
                                >
                                    {(function() {
                                        if (this.state.isDeleting) {
                                            return this._renderIsLoading();
                                        }

                                        return (<i className="fa fa-remove" />);
                                    }).bind(this)()}
                                </button>
                            </span>
                        </div>
                    </header>

                    <summary
                        className="row"
                        dangerouslySetInnerHTML={this._parseMarkdown(this.props.note.body)}
                        style={this.styles.textField}
                    />

                    {this._renderNoteTags()}
                </div>
            </article>
        );
    },

    /**
     * @returns {XML}
     * @private
     */
    _renderForm: function() {
        var titleValue;
        var bodyValue;
        var tagsValue;

        if (this.props.note) {
            titleValue = this.props.note.title;
            bodyValue = this.props.note.body;
            tagsValue = this.props.note.tags.join(',');
        }

        return (
            <article className="formNote col-lg-3 col-md-4" style={this.styles.article}>
                <form className="container" style={this.styles.articleBody}>
                    <header className="row">
                            <span className="col-lg-10 col-md-10 col-sm-11 col-xs-11">
                                <input
                                    ref="titleInput"
                                    className="form-control"
                                    type="text"
                                    placeholder={this.formPlaceholders.title}
                                    defaultValue={titleValue}
                                    style={this.styles.form.title}

                                    onKeyPress={this._onKeyPress}
                                />
                            </span>
                        <div className="col-lg-2 col-md-2 col-sm-1 col-xs-1" style={this.styles.btnGroupWrapper}>
                                <span className="pull-right">
                                    {(function() {
                                        if (this.props.formOnly) {
                                            return (
                                                <button
                                                    type="button"
                                                    className="createBtn btn btn-success"
                                                    style={this.styles.button}

                                                    onClick={this._onCreateClick}
                                                >
                                                    {(function() {
                                                        if (this.state.isCreating) {
                                                            return this._renderIsLoading();
                                                        }

                                                        return (<i className="fa fa-plus" />)
                                                    }).bind(this)()}
                                                </button>
                                            )
                                        }

                                        return (
                                            <button
                                                type="button"
                                                className="confirmEditBtn btn btn-success"
                                                style={this.styles.button}

                                                onClick={this._onConfirmEditClick}
                                            >
                                                {(function() {
                                                    if (this.state.isEditing) {
                                                        return this._renderIsLoading();
                                                    }

                                                    return (<i className="fa fa-check" />);
                                                }).bind(this)()}
                                            </button>
                                        );
                                    }).bind(this)()}
                                </span>
                        </div>
                    </header>

                    <div className="row">
                            <span className="col-xs-12">
                                <textarea
                                    ref="bodyInput"
                                    className="form-control"
                                    placeholder={this.formPlaceholders.body}
                                    defaultValue={bodyValue}

                                    style={this.styles.form.body}
                                />
                            </span>
                            <span className="col-xs-12">
                                <input
                                    ref="tagsInput"
                                    className="form-control"
                                    type="text"
                                    placeholder={this.formPlaceholders.tags}
                                    defaultValue={tagsValue}
                                    style={this.styles.form.tags}

                                    onKeyPress={this._onKeyPress}
                                />
                            </span>
                    </div>
                </form>
            </article>
        );
    },

    /**
     * @returns {boolean|XML}
     * @private
     */
    _renderNoteTags: function() {
        if (!this.props.note || !this.props.note.tags) {
           return false;
        }

        return (
            <h6>
                {this.props.note.tags.map(function(tag) {return (
                    <span key={tag} className="label label-default" style={this.styles.tag}>{tag}</span>
                )}.bind(this))}
            </h6>
        );
    },

    /**
     * @returns {XML}
     * @private
     */
    _renderIsLoading: function() {
        return (<i className="fa fa-spin fa-cog" />);
    },

    /**
     * @returns {boolean}
     * @private
     */
    _onCreateClick: function() {
        var title;
        var body;
        var tags;

        if (!this.props.formOnly || this.state.isCreating) {
            return false;
        }

        this.setState({isCreating: true});

        title = this.refs.titleInput.value;
        body = this.refs.bodyInput.value;
        tags = this.refs.tagsInput.value;

        NoteCollection.createNote(title, body, tags)
            .then(function() {
                this.setState({isCreating: false});

                // Resetting inputs
                this.refs.titleInput.value = '';
                this.refs.bodyInput.value = '';
                this.refs.tagsInput.value = '';
            }.bind(this))
            .catch(function(errors) {
                this.setState({isCreating: false});

                this._throwErrorNotifications(errors);
            }.bind(this));

        return true;
    },

    /**
     * @returns {boolean}
     * @private
     */
    _onConfirmEditClick: function() {
        var title;
        var body;
        var tags;

        if (!this.state.editMode || this.state.isEditing) {
            return false;
        }

        this.setState({isEditing: true});

        title = this.refs.titleInput.value;
        body = this.refs.bodyInput.value;
        tags = this.refs.tagsInput.value;

        NoteCollection.updateNote(this.props.note.id, title, body, tags)
            .then(function() {
                this.setState({isEditing: false, editMode: false});
            }.bind(this))
            .catch(function(errors) {
                this.setState({isEditing: false});

                this._throwErrorNotifications(errors);
            }.bind(this));

        return true;
    },

    /**
     * @returns {boolean}
     * @private
     */
    _onEditClick: function() {
        if (this.state.editMode || this.state.isEditing) {
            return false;
        }

        this.setState({editMode: true});
        return true;
    },

    /**
     * @returns {boolean}
     * @private
     */
    _onDeleteClick: function() {
        if (this.state.isDeleting) {
            return false;
        }

        this.setState({isDeleting: true});

        NoteCollection.deleteNote(this.props.note.id)
            .catch(function(errors) {
                this.setState({isDeleting: false});

                this._throwErrorNotifications(errors);
            }.bind(this));

        return true;
    },

    /**
     * @param {event} evt
     * @returns {boolean}
     * @private
     */
    _onKeyPress: function(evt) {
        if (evt.key === 'Enter') {
            if (this.state.editMode) {
                return this._onConfirmEditClick(evt);
            }

            if (this.props.formOnly) {
                return this._onCreateClick(evt);
            }

            return false;
        }

        return false;
    },

    /**
     *
     * @param errors
     * @returns {boolean}
     * @private
     */
    _throwErrorNotifications: function(errors) {
        if (errors && Array.isArray(errors)) {
            _.forEach(errors, function(error) {
                this.props.notificationSystem.addNotification({
                    title: 'Error',
                    message: error.message,
                    level: 'error'
                });
            }.bind(this))
        } else {
            this.props.notificationSystem.addNotification({
                title: 'Internal Server Error',
                message: 'Something went wrong with our hamsters...',
                level: 'error'
            });
        }

        return true;
    },

    /**
     * @param {string} value
     * @returns {{__html: *}}
     * @private
     */
    _parseMarkdown: function(value) {
        var markup = marked(value, {sanitize: true});
        return {__html: markup};
    }
});

module.exports = Note;
