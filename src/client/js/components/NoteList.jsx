'use strict';

var React = require('react');
var NotificationSystem = require('react-notification-system');

var Note = require('app/components/Note');

var NoteList = React.createClass({
    propTypes: {
        notes: React.PropTypes.array.isRequired
    },

    styles: {
        list: {
            padding: 0
        }
    },

    /**
     * @returns {{notificationSystem: null}}
     */
    getInitialState: function() {
        return {
            notificationSystem: null
        };
    },

    /**
     * @returns {boolean}
     */
    componentDidMount: function() {
        this.setState({
            notificationSystem: this.refs.notificationSystem
        });

        return true;
    },

    /**
     * @returns {XML}
     */
    render: function() {
        return (
            <ol className="row" style={this.styles.list}>
                <NotificationSystem ref="notificationSystem" />

                <Note formOnly notificationSystem={this.state.notificationSystem} />

                {this.props.notes.map(function(note) {return (
                    <Note key={note.id} note={note} notificationSystem={this.state.notificationSystem} />
                )}.bind(this))}
            </ol>
        );
    }
});

module.exports = NoteList;
