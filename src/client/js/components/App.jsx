'use strict';

var React = require('react');
var BackboneReact = require('backbone-react-component');
var _ = require('lodash');

var NoteCollection = require('app/collections/NoteCollection');
var SearchModel= require('app/models/globals/SearchModel');

var NoteList = require('app/components/NoteList');
var Header = require('app/components/Header');

/**
 * Relative path for usage in tests and server
 * @TODO Package the utils
 */
var TagUtils = require('../../../utils/tag');

var App = React.createClass({
    /**
     * @returns {{notes: Array, search: null}}
     */
    getInitialState: function() {
        return {
            notes: NoteCollection.toJSON(),
            search: SearchModel.toJSON()
        };
    },

    /**
     * @returns {boolean}
     */
    componentWillMount: function() {
        BackboneReact.on(this, {
            collections: {
                notes: NoteCollection
            },
            models: {
                search: SearchModel
            }
        });

        return true;
    },

    /**
     * @returns {boolean}
     */
    componentWillUnmount: function() {
        BackboneReact.off(this);

        return true;
    },

    /**
     * @returns {XML}
     */
    render: function() {
        return (
            <main className="container">
                <Header />
                <NoteList notes={this._filterNotes(this.state.notes, this.state.search.searchFilter)} />
            </main>
        );
    },

    /**
     * Filters the NoteCollection by user-defined tags
     *
     * @param {array} notes
     * @param {string} searchQuery
     * @returns {array}
     * @private
     */
    _filterNotes: function(notes, searchQuery) {
        var filteredNotes;
        var tags;

        if (!searchQuery) {
            return notes;
        }

        tags = TagUtils.expandAndNormalize(searchQuery);
        if (!tags.length) {
            return notes;
        }

        filteredNotes = [];
        _.forEach(notes, function(note) {
            var isExcluded = false;

            _.forEach(tags, function(tag) {
                if (note.tags.indexOf(tag) === -1) {
                    isExcluded = true;
                    return false;
                }

                return true;
            });

            if (!isExcluded) {
                filteredNotes.push(note);
            }
        });

        return filteredNotes;

    }
});

module.exports = App;
