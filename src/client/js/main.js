'use strict';

var React = require('react');
var ReactDOM = require('react-dom');

var App = require('app/components/App');

/**
 * SCSS VENDORS
 */
require('scss/bootstrap.scss');
require('scss/font-awesome.scss');

/**
 * APP RENDER
 */
ReactDOM.render(<App />, document.getElementById('app'));
