'use strict';

var Backbone = require('backbone');
var _ = require('lodash');
var request = require('superagent');
var Promise = require('bluebird');

var NoteModel = require('app/models/NoteModel');

/**
 * Relative path for usage in tests and server
 * @TODO Package the utils
 */
var TagUtils = require('../../../utils/tag');
var ConfigUtils = require('../../../utils/config');

var NoteCollection = Backbone.Collection.extend({model: NoteModel});
_.extend(NoteCollection.prototype, {
    url: ConfigUtils.api.url + '/notes',

    /**
     * Bootstrap the Collection and sync the notes
     *
     * @returns {*}
     */
    initialize: function() {
        var result;

        result = Backbone.Collection.prototype.initialize.call(this);
        this.syncNotes();

        return result;
    },

    /**
     * Fetches notes from the API and replaces current notes
     *
     * @returns {Promise}
     */
    syncNotes: function() {
        return new Promise(function(resolve, reject) {
            request.get(this.url)
                .end(function(err, res) {
                    if (err) {
                        return reject();
                    }

                    if (res.body.success === false) {
                        return reject(res.body.errors);
                    }

                    if (!Array.isArray(res.body.data)) {
                        return reject();
                    }

                    this.reset();
                    for (var i = 0; i < res.body.data.length; i++) {
                        this.add(new this.model(res.body.data[i]));
                    }

                    return resolve(this.toJSON());
                }.bind(this));
        }.bind(this));
    },

    /**
     * @param {string} title
     * @param {string} body
     * @param {string} tags
     * @returns {Promise}
     */
    createNote: function(title, body, tags) {
        return new Promise(function(resolve, reject) {
            var tempNote;
            var validation;

            tempNote = new this.model({
                title: title,
                body: body,
                tags: TagUtils.expandAndNormalize(tags)
            });

            validation = tempNote.validate();
            if (validation !== true) {
                return reject(validation);
            }

            request.post(this.url)
                .send(tempNote.toJSON())
                .end(function(err, res) {
                    if (err) {
                        return reject();
                    }

                    if (res.body.success === false) {
                        return reject(res.body.errors);
                    }

                    this.unshift(new this.model(res.body.data));
                    return resolve(this.get(res.body.data.id));
                }.bind(this));
        }.bind(this));
    },

    /**
     * @param {string} id
     * @param {string} title
     * @param {string} body
     * @param {Array} tags
     * @returns {Promise}
     */
    updateNote: function(id, title, body, tags) {
        return new Promise(function(resolve, reject) {
            var tempNote;
            var validation;

            tempNote = new this.model({
                id: id,
                title: title,
                body: body,
                tags: TagUtils.expandAndNormalize(tags)
            });

            validation = tempNote.validate();
            if (validation !== true) {
                return reject(validation);
            }

            request.put(this.url + '/' + id)
                .send(tempNote.toJSON())
                .end(function(err, res) {
                    if (err) {
                        return reject();
                    }

                    if (res.body.success === false) {
                        return reject(res.body.errors);
                    }

                    this.get(res.body.data.id).set(res.body.data);
                    return resolve(this.get(res.body.data.id));
                }.bind(this));
        }.bind(this));
    },

    /**
     * @param {string} id
     * @returns {Promise}
     */
    deleteNote: function(id) {
        return new Promise(function(resolve, reject) {
            request.del(this.url + '/' + id)
                .end(function(err, res) {
                    if (err) {
                        return reject();
                    }

                    if (res.success === false) {
                        return reject(res.body.errors);
                    }

                    this.remove(id);
                    return resolve();
                }.bind(this));
        }.bind(this));
    }
});

module.exports = new NoteCollection();
