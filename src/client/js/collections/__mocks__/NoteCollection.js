'use strict';

var Backbone = require('backbone');
var _ = require('lodash');
var Promise = require('bluebird');

var NoteModel = require('app/models/NoteModel');

var NoteCollection = Backbone.Collection.extend({model: NoteModel});
_.extend(NoteCollection.prototype, {
    initialize: function() {
        var result;

        result = Backbone.Collection.prototype.initialize.call(this);

        this.add(new this.model({
            id: 'mock',
            title: 'mock',
            body: 'mock',
            tags: ['mock1', 'mock3']
        }));

        this.add(new this.model({
            id: 'mock2',
            title: 'mock',
            body: 'mock',
            tags: ['mock2', 'mock4']
        }));

        return result;
    },

    createNote: jest.genMockFunction().mockImplementation(function() {
        return new Promise(function(resolve, reject) {
            resolve();
        });
    }),

    updateNote: jest.genMockFunction().mockImplementation(function() {
        return new Promise(function(resolve, reject) {
            resolve();
        });
    }),

    deleteNote: jest.genMockFunction().mockImplementation(function() {
        return new Promise(function(resolve, reject) {
            resolve();
        });
    })
});

module.exports = new NoteCollection();
