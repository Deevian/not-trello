'use strict';

var ConfigUtils = {
    api: {
        url: 'https://feedzai.deev.io/api/v1',
        relativeUrl: '/api/v1'
    },
    db: {
        name: 'feedzai',
        table: 'notes'
    }
};

module.exports = ConfigUtils;
