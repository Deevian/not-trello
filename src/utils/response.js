'use strict';

var ResponseUtils = {};

/**
 * @param {*} data
 * @param {boolean} success
 * @param {int} code
 * @param {array} errors
 * @returns {{data: *, success: boolean, code: int, error: *}}
 */
ResponseUtils.generateResponse = function(data, success, code, errors) {
    return {
        data: data,
        success: success,
        code: code,
        errors: errors
    };
};

module.exports = ResponseUtils;
