'use strict';

var _ = require('lodash');

var ValidatorUtils = {};

/**
 * @param {string} name
 * @param {string} value
 * @param {object} conditions
 * @returns {boolean|{field: string, message: string}}
 */
ValidatorUtils.validateStringField = function(name, value, conditions) {
    var minLength;
    var maxLength;

    if (typeof value !== 'string') {
        return _generateFieldError(name, _.capitalize(name) + ' must be of type `string`.');
    }

    if (!conditions) {
        return true;
    }

    if (conditions.minLength !== null) {
        minLength = conditions.minLength;
        if (value.length < minLength) {
            if (minLength === 1) {
                return _generateFieldError(name, _.capitalize(name) + ' cannot be empty.');
            }

            return _generateFieldError(name, _.capitalize(name) + ' must be larger than ' + minLength + ' characters.');
        }
    }

    if (conditions.maxLength !== null) {
        maxLength = conditions.maxLength;
        if (value.length > maxLength) {
            return _generateFieldError(name, _.capitalize(name) + ' cannot be larger than ' + maxLength + ' characters.');
        }

    }

    return true;
};

/**
 * @param {string} fieldName
 * @param {string} message
 * @returns {{field: string, message: string}}
 * @private
 */
function _generateFieldError(fieldName, message) {
    return {field: fieldName, message: message};
}

module.exports = ValidatorUtils;
