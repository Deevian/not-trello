'use strict';

var _ = require('lodash');

var TagUtils = {};

/**
 * Receives a comma-separated tag string and return formatted tag array
 *
 * @param {string} tags
 * @returns {array}
 */
TagUtils.expandAndNormalize = function(tags) {
    if (!tags) {
        return [];
    }

    tags = this.expand(tags);
    tags = this.normalizeExpanded(tags);

    return tags;
};

/**
 * @param {string} tags
 * @returns {array}
 */
TagUtils.expand = function(tags) {
    if (!tags) {
        return [];
    }

    return tags.split(',');
};

/**
 * @param {array} tags
 * @returns {array}
 */
TagUtils.normalizeExpanded = function(tags) {
    tags = _.map(tags, _.trim);
    tags = _.map(tags, Function.call.bind("".toLowerCase));
    tags = _.uniq(tags);

    return tags;
};

module.exports = TagUtils;
