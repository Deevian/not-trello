'use strict';

var path = require('path');
var r = require('rethinkdb');
var uuid = require('node-uuid');

var NoteModel = require('../client/js/models/NoteModel');

var ConfigUtils = require('../utils/config');
var ResponseUtils = require('../utils/response');
var TagUtils = require('../utils/tag');

var routes = function(app) {
    app.route(ConfigUtils.api.relativeUrl + '/notes')
        /**
         * GET NOTES
         */
        .get(function(req, res) {
            var conn;
            var response;

            r.connect({db: ConfigUtils.db.name})
                .then(function(connection) {
                    conn = connection;
                    return r.table(ConfigUtils.db.table).orderBy(r.desc('published')).run(conn);
                })
                .then(function(cursor) {
                    // r.table returns a cursor, so we convert it to an array for consistency
                    return cursor.toArray();
                })
                .then(function(results) {
                    conn.close();

                    response = ResponseUtils.generateResponse(results, true, 200);
                    return res.json(response);
                })
                .catch(function(err) {
                    conn.close();
                    console.error(err);

                    response = ResponseUtils.generateResponse(null, false, 500);
                    return res.json(response);
                })
        })

        /**
         * CREATE NOTE
         */
        .post(function(req, res) {
            var conn;
            var response;

            var tempNote;
            var errors;

            // Request must include a body
            if (!req.body) {
                response = ResponseUtils.generateResponse(null, false, 400);
                return res.json(response);
            }

            // Mock the model and validate it
            tempNote = {
                title: req.body.title,
                body: req.body.body,
                tags: TagUtils.normalizeExpanded(req.body.tags)
            };

            tempNote = new NoteModel(tempNote);
            errors = tempNote.validate();

            // If there are validation errors, bounce them back to the client
            if (Array.isArray(errors)) {
                response = ResponseUtils.generateResponse(null, false, 400, errors);
                return res.json(response);
            }

            // Generate the model id and set the timestamps
            tempNote.set('id', uuid.v1());
            tempNote.set('published', new Date().getTime());
            tempNote.set('updated', tempNote.get('published'));

            // For testing purposes
            if (req.body.mock) {
                response = ResponseUtils.generateResponse(tempNote.toJSON(), true, 201);
                return res.json(response);
            }

            r.connect({db: ConfigUtils.db.name})
                .then(function(connection) {
                    conn = connection;
                    return r.table(ConfigUtils.db.table).insert(tempNote.toJSON()).run(conn);
                })
                .then(function(status) {
                    // Check if note was inserted
                    if (status.inserted !== 1) {
                        response = ResponseUtils.generateResponse(null, false, 500);
                        return res.json(response);
                    }

                    response = ResponseUtils.generateResponse(tempNote.toJSON(), true, 201);
                    return res.json(response);
                })
                .catch(function(err) {
                    conn.close();
                    console.error(err);

                    response = ResponseUtils.generateResponse(null, false, 500);
                    return res.json(response);
                })
        });

    app.route(ConfigUtils.api.relativeUrl + '/notes/:id')
        /**
         * GET NOTE
         */
        .get(function(req, res) {
            var conn;
            var response;

            r.connect({db: ConfigUtils.db.name})
                .then(function(connection) {
                    conn = connection;
                    return r.table(ConfigUtils.db.table).get(req.params.id).run(conn);
                })
                .then(function(note) {
                    if (!note) {
                        response = ResponseUtils.generateResponse(null, false, 404);
                        return res.json(response);
                    }

                    response = ResponseUtils.generateResponse(note, true, 200);
                    return res.json(response);
                })
                .catch(function(err) {
                    conn.close();
                    console.error(err);

                    response = ResponseUtils.generateResponse(null, false, 500);
                    return res.json(response);
                });
        })

        /**
         * UPDATE NOTE
         */
        .put(function(req, res) {
            var conn;
            var response;

            var tempNote;
            var errors;

            // Request must include a body
            if (!req.body) {
                response = ResponseUtils.generateResponse(null, false, 400);
                return res.json(response);
            }

            // Mock the model and validate it
            tempNote = {
                id: req.params.id,
                title: req.body.title,
                body: req.body.body,
                tags: TagUtils.normalizeExpanded(req.body.tags)
            };

            tempNote = new NoteModel(req.body);
            errors = tempNote.validate();

            // If there are validation errors, bounce them back to the client
            if (Array.isArray(errors)) {
                response = ResponseUtils.generateResponse(null, false, 400, errors);
                return res.json(response);
            }

            // Set new updated timestamp
            tempNote.unset('published');
            tempNote.set('updated', new Date().getTime());

            // For testing purposes
            if (req.body.mock) {
                response = ResponseUtils.generateResponse(tempNote.toJSON(), true, 200);
                return res.json(response);
            }

            r.connect({db: ConfigUtils.db.name})
                .then(function(connection) {
                    conn = connection;
                    return r.table(ConfigUtils.db.table).get(req.params.id).update(tempNote.toJSON()).run(conn);
                })
                .then(function(status) {
                    // Check if note was updated
                    if (status.replaced !== 1) {
                        response = ResponseUtils.generateResponse(null, false, 500);
                        return res.json(response);
                    }

                    response = ResponseUtils.generateResponse(tempNote.toJSON(), true, 200);
                    return res.json(response);
                })
                .catch(function(err) {
                    conn.close();
                    console.error(err);

                    response = ResponseUtils.generateResponse(null, false, 500);
                    return res.json(response);
                })
        })

        /**
         * DELETE NOTE
         */
        .delete(function(req, res) {
            var conn;
            var response;

            // For testing purposes
            if (req.body.mock) {
                response = ResponseUtils.generateResponse(null, true, 200);
                return res.json(response);
            }

            r.connect({db: ConfigUtils.db.name})
                .then(function(connection) {
                    conn = connection;
                    return r.table(ConfigUtils.db.table).get(req.params.id).delete().run(conn);
                })
                .then(function(status) {
                    // Check if note was deleted
                    if (status.deleted !== 1) {
                        response = ResponseUtils.generateResponse(null, false, 500);
                        return res.json(response);
                    }

                    response = ResponseUtils.generateResponse(null, true, 200);
                    return res.json(response);
                })
                .catch(function(err) {
                    conn.close();
                    console.error(err);

                    response = ResponseUtils.generateResponse(null, false, 500);
                    return res.json(response);
                })
        });
};

module.exports = routes;
