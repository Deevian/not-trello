#!/usr/bin/env node
'use strict';

var r = require('rethinkdb');
var conn;

r.connect()
    .then(function(connection) {
        conn = connection;
        return r.dbCreate('feedzai').run(conn);
    })
    .then(function() {
        return r.db('feedzai').tableCreate('notes').run(conn);
    })
    .then(function() {
        console.log('Done!');
    });
