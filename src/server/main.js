'use strict';

var path = require('path');

/**
 * EXPRESS & SOCKET.IO SERVER
 */
var express = require('express');
var app = express();
var server = require('http').Server(app);
var cors = require('cors');

/**
 * EXPRESS MIDDLEWARE REQUIRE
 */
var bodyParser = require('body-parser');
var logger = require('morgan');

/**
 * APP PATHS
 */
app.paths = {
    base: path.resolve(__dirname, '..', '..'),
    src: path.resolve(__dirname, '..')
};

/**
 * EXPRESS MIDDLEWARE USAGE
 */
app.use(logger('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/dist', express.static(path.resolve(app.paths.base, 'dist'), {
    etag: false
}));

app.use(cors());

/**
 * SERVE INDEX
 */
app.get('/', function(req, res) {
    res.sendFile(path.resolve(app.paths.base, 'index.html'));
});

/**
 * REQUIRE ROUTES
 */
require('./routes.js')(app);

/**
 * ACCEPT CONNECTIONS FROM PORT
 */
server.listen(4444);
