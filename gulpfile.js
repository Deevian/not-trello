'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');

var paths = require('./build/paths');

var environment;

process.env.ENV_PROD = 'production';
process.env.ENV_DEV = 'development';

/**
 * ENVIRONMENT
 */
environment = gutil.env.env ? gutil.env.env : gutil.env.environment;
environment = typeof environment !== 'undefined' ? environment : process.env.ENV_DEV;

switch (environment.toLowerCase()) {
    case 'production':
    case 'prod':
        environment = process.env.ENV_PROD;
        break;
    default:
        environment = process.env.ENV_DEV;
        break;
}

process.env.NODE_ENV = environment;

/**
 * REQUIRE BUILD TASKS
 */
require('require-dir')(paths.build_tasks);

/**
 * DEFAULT TASK
 */
gulp.task('default', function(callback) {
    runSequence('clean', 'build', callback);
});
