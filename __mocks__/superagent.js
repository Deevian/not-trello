'use strict';

jest.dontMock('../src/utils/response');

var _ = require('lodash');
var ResponseUtils = require('../src/utils/response');

var mockResponse;
var mockNote1 = {
    id: 'mock',
    title: 'mock',
    body: 'mock',
    tags: ['mock', 'mock1']
};
var mockNote2 = {
    id: 'mock1',
    title: 'mock1',
    body: 'mock',
    tags: ['mock', 'mock1']
};

var Request = {
    get: jest.genMockFunction().mockImplementation(function(endpoint) {
        endpoint = endpoint.split(',');

        if (typeof endpoint[endpoint.length-1] === 'number') {
            mockResponse = ResponseUtils.generateResponse(mockNote1, true, 200);
            return this;
        }

        mockResponse = ResponseUtils.generateResponse([mockNote1, mockNote2], true, 200);
        return this;
    }),

    post: jest.genMockFunction().mockImplementation(function() {
        return this;
    }),

    put: jest.genMockFunction().mockImplementation(function() {
        return this;
    }),

    del: jest.genMockFunction().mockImplementation(function() {
        mockResponse = ResponseUtils.generateResponse(null, true, 200);
        return this;
    }),

    send: jest.genMockFunction().mockImplementation(function(mockNote) {
        var mock = _.clone(mockNote);

        mock.id = 'mock';
        mockResponse = ResponseUtils.generateResponse(mock, true, 200);

        return this;
    }),

    end: jest.genMockFunction().mockImplementation(function(callback) {
        var requestResponse;

        requestResponse = {body: mockResponse, err: null};
        callback(null, requestResponse);

        return true;
    })
};

module.exports = Request;
