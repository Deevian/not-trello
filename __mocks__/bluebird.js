'use strict';

/**
 * Jest also freaks out with bluebird, so we make this workaround
 */

jest.autoMockOff();
module.exports = require.requireActual('bluebird');
jest.autoMockOn();
