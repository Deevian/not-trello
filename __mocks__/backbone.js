'use strict';

/**
 * Jest freaks out with backbone, so we make this workaround
 */

jest.autoMockOff();
module.exports = require.requireActual('backbone');
jest.autoMockOn();
