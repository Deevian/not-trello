'use strict';

/**
 * Jest does NOT freak out with lodash, but we never want to mock it anyway
 */

jest.autoMockOff();
module.exports = require.requireActual('lodash');
jest.autoMockOn();
