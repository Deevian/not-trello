# Note Board #

===============

Note Board is a JavaScript application developed to solve the Feedzai aptitude test #24. It was coded in under a week, mostly during after-work hours (while Fallout 4 and Rocket League sat in the corner of the room, feeling lonely).

### Feature rundown ###

* Online Post-it Board
* Server backend
* Markdown support
* Tag support

### Front-End Stack Overview ###

* [Webpack](https://webpack.github.io/)
* [Gulp](http://gulpjs.com/)
* [Backbone](http://backbonejs.org/)
* [React](https://facebook.github.io/react/)
* [Bootstrap](http://v4-alpha.getbootstrap.com/)
* [Font-Awesome](https://fortawesome.github.io/Font-Awesome/)

### Back-End Stack Overview ###

* [Node](https://nodejs.org/en/)
* [Express](http://expressjs.com/)
* [RethinkDB](https://www.rethinkdb.com/)

### QUICK SETUP ###

```
# npm install
# ./node_modules/gulp/bin/gulp serve --env prod
```

### RUNNING TESTS ###

```
# npm test
```

### LIVE EXAMPLE ###

* [https://feedzai.deev.io/](https://feedzai.deev.io/)

### TODO ###

* Socket support (getting around CloudFlare SSL is proving to be a pain, but nothing that cannot be done)