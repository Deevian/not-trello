'use strict';

var _ = require('lodash');
var path = require('path');
var babelJest = require('babel-jest');
var webpackAlias = require('jest-webpack-alias');

var dirs = ['../src', '../__tests__'].map(function(dir) {
    return path.resolve(__dirname, dir);
});

function matches(filename) {
    return _.find(dirs, function(dir) {
        return filename.indexOf(dir) === 0;
    });
}

module.exports = {
    process: function(src, filename) {
        if (matches(filename)) {
            // Fix the Jest & Webpack alias issue by replacing the requires with a valid path
            src = src.replace(/require\(\'app/gm, 'require(\'src/client/js');

            src = webpackAlias.process(src, filename);
            src = babelJest.process(src, filename);
        }

        return src;
    }
};
